
# Mômji Backend projet
Projet qui permet à l'utilisateur de visionner les données liées à une liste de produit sous forme de tableau, de les exporter en un fichier excel et de les importer en utilisant la librairie DataTables et Laravel Excel.

Les données seront affichées comme ceci :

| Title   | Price | VAT   | Created_at          | Updated_at          |
|---------|-------|-------|---------------------|---------------------|
| Product | 200   | 20.50 | 2023-02-02 xx:xx:xx | 2023-02-02 xx:xx:xx |
| Product | 200   | 20.50 | 2023-02-02 xx:xx:xx | 2023-02-02 xx:xx:xx |
| Product | 200   | 20.50 | 2023-02-02 xx:xx:xx | 2023-02-02 xx:xx:xx |


## Prérequis
La librairie Laravel Excel à besoin des extentions suivantes active sur l'environnement
- PHP: ^7.2\|^8.0
- Laravel: ^5.8
- PhpSpreadsheet: `^1.21
- PHP extension php_zip 
- PHP extension php_xml 
- PHP extension php_gd2 
- PHP extension php_iconv 
- PHP extension php_simplexml 
- PHP extension php_xmlreader 
- PHP extension php_zlib 

# Installation
Utilisez la commande composer afin d'installer les dépendances du projet.
> composer i

Renommez le fichier .env.example en .env

Générer une nouvelle api_key pour l'application avec la commande suivante :
> php artisan key:generate

Créer une base de donnée nommée momji_back_end


### Ajout des données 
Vous pouvez générer les champs en base la commande suivante :


> php artisan migrate


Utilisez le service Seeder afin de générer des données en base

> php artisan db:seed --class=ProductSeeder

Le serveur est prêt pour démarrer

> php artisan serve

## Listage des routes

| URI     | Méthode | Description                                     |
|---------|---------|-------------------------------------------------|
| /       | GET     | Page d'accueil                                  |
| /index  | GET     | Liste des entrées en base                       |
| /export | GET     | Export de la base en un fichier excel           |
| /import | POST    | Import d'un fichier excel pour mise à jour base |


En cas de lancement d'une commande de test, relancer le script de seed afin de réinsérer des données en base

> php artisan test

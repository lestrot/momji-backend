<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <style>
        *{
            font-family: 'Nunito', sans-serif;
        }
        body {
            background-color: #ecf0f5;
            margin: 0;
        }
        section{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        section:last-child{
            padding-top: 10vh;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
            border-bottom: 1px solid #ddd;
        }
        td:hover{
            background-color: #daebf5;
        }

        th {
            background-color: #338fa2;
            color: white;
        }
        h1{
            font-weight: bolder;
            font-size: 2.5em;
            margin: 20px 0 0 0;
        }
        .manage-content{
            background-color: #338fa2;
            margin-top: 40px;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 2px 2px 3px cadetblue;
        }
        .manage-content, .manage-content form{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .manage-content *{
            margin: 2px 0;
        }
        a{
            text-decoration: none;
            text-underline: none;
            color: #7f8ebb;
        }
        a:visited{
            display: none;
        }
        button, input{
            background-color: #f0f5fa;
            border: none;
            color: #7f8ebb;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            border-radius: 5px;
        }
        input{
            padding: 15px 10px;
        }
        .message{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .message div {
            margin: 7px 0 7px 0;
            border-radius: 5px;
        }
        .message p{
            padding: 0 8px;
        }
        .success{
            background-color: lightgreen;
        }
        .error{
            background-color: coral;
        }
    </style>

</head>
<body>

<div class="message">
    <h1>Products list</h1>
    @if (session('success'))
        <div class="success">
            <p>{{ session('success') }}</p>
        </div>
    @endif
    @if (session('error'))
        <div class="error">
            <p>{{ session('error') }}</p>
        </div>
    @endif
</div>

<section>
    <table id="product-table">
        <thead>
        <tr>
            <th>Title</th>
            <th>Price</th>
            <th>VAT</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        </thead>
    </table>
    <div class="manage-content">
        <button><a href="{{ route('export') }}" class="btn btn-primary">Download Products</a></button>
        <form method="POST" action="{{ route('import') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input type="file" class="form-control-file" id="excel-file" name="excel-file">
            </div>
            <button type="submit" class="">Import</button>
        </form>

    </div>

</section>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        $('#product-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('products.index') }}',
            columns: [
                { data: 'productTitle', name: 'title' },
                { data: 'price', name: 'price' },
                { data: 'vat', name: 'vat' },
                {
                    data: 'created_at',
                    name: 'created_at',
                    render: function(data) {
                        return moment(data).format('YYYY-MM-DD HH:mm:ss');
                    }
                },
                {
                    data: 'updated_at',
                    name: 'updated_at',
                    render: function(data) {
                        return moment(data).format('YYYY-MM-DD HH:mm:ss');
                    }
                }
            ]
        });
    });
</script>
</body>
</html>

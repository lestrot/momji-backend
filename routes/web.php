<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});
Route::get('/index', [ProductController::class, 'getEntries'])->name('products.index');
Route::get('/export', [ProductController::class, 'export'])->name('export');
Route::post('/import', [ProductController::class, 'import'])->name('import');

<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Imports\ProductImport;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Exceptions\NoTypeDetectedException;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    public function getEntries()
    {
        $products = Product::all();

        return DataTables::of($products)->make(true);
    }

    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function import(Request $request){
        if (!$request->hasFile('excel-file')) {
            return redirect()->back()->with('error', 'Please select a file to import.');
        }
        try {
            try {
                $file = $request->file('excel-file');

                Excel::import(new ProductImport, $file);
                return redirect('/')->with('success', 'import success');
            }catch (QueryException $queryException){
                return redirect('/')->with('error', 'invalid excel data format');
            }catch(\Exception $exception){
               if($exception->getMessage() == 'Undefined array key 1'){
                   return redirect('/')->with('error', 'Empty file, can\'t proceed');
               }else{
                   return redirect('/')->with('error', 'Error during file process : ' . $exception->getMessage());
               }
            }
        }catch (NoTypeDetectedException $exception){
            return redirect('/')->with('error', 'incorrect file type provided');
        }

    }
}

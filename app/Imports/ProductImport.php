<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $product = Product::where('id', $row[0])->first();
        if ($product) {
            $product->update([
                'productTitle' => $row[1],
                'price' => $row[2],
                'vat' => $row[3],
            ]);
        }else {
            $product = new Product([
                'productTitle' => $row[1],
                'price' => $row[2],
                'vat' => $row[3],
            ]);
            $product->save();
        }
        return null;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'productTitle',
        'price',
        'vat',
        'updated_at',
    ];

    use HasFactory;
}

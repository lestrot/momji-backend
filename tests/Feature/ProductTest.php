<?php

namespace Tests\Feature;

use App\Imports\ProductImport;
use App\Models\Product;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;


class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_all_entries_from_database()
    {
        $faker = Factory::create();
        $products = Product::create([
            'productTitle' => $faker->unique()->words(3, true),
            'price' => $faker->randomFloat(2,1,875),
            'vat' => $faker->randomFloat(2,0,30),
            'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
            'updated_at' => $faker->dateTimeBetween('-1 year', 'now')
        ]);


        $response = $this->get(route('products.index'));

        $response->assertSuccessful();

        foreach ($products as $product) {
            $response->assertSee(isset($product->productTitle));
            $response->assertSee(isset($product->price));
            $response->assertSee(isset($product->vat));
        }
    }
    public function test_download_current_entries_to_excel()
    {

        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/export');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->assertHeader('Content-Disposition', 'attachment; filename=products.xlsx');

    }
//    public function test_import_a_excel_dumped_file()
//    {
//        Excel::fake();
//        $user = User::factory()->create();
//        $this->actingAs($user)->get('/import');
//        Excel::assertImported('products.xlsx', 'local', function (ProductImport $import){
//            return true;
//        });
//    }
}
